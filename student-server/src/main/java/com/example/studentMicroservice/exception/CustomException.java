package com.example.studentMicroservice.exception;

public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public String customMessage="Unknown_Exception";
	public CustomException(String arg0, String arg1) {
		super(arg0);
		this.customMessage = arg1;
	}

	public CustomException(String arg0) {
		super(arg0);
	}

	public CustomException(Throwable arg0) {
		super(arg0);
	}	

}

