package com.example.studentMicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.example.studentMicroservice.entity.Student;

public interface StudentRepository extends CrudRepository<Student, Integer> {
	@Query(value = "SELECT * FROM student where name = :name", nativeQuery = true)
	public List<Student> findStudentsByName(@Param("name") String name);
}
