package com.example.studentMicroservice.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.studentMicroservice.entity.Student;
import com.example.studentMicroservice.exception.CustomException;
import com.example.studentMicroservice.service.StudentService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value="/api/students")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@RequestMapping(value = "/getAllStudents", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Student>> getAllStudents() {
		List<Student> students = null;
		try 
		{
			students = studentService.getAllStudents();
		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage(),"can not get all students");
		}
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}

	@RequestMapping(value = "getStudentDetail/{student_id}", method = RequestMethod.GET)
	public ResponseEntity<Student> getStudentDetail(@PathVariable("student_id") Integer student_id){
		Student student = null;
		try 
		{
			student = studentService.getStudentDetails(student_id);

		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage(),"get studentDetail failed");
		}
		return new ResponseEntity<Student>(student, HttpStatus.OK); 
	}

	@RequestMapping(value="/addStudent",method=RequestMethod.POST,produces="application/json")
	public ResponseEntity <Student> addStudent(@RequestBody Student student) {
		Student addStudent = null;
		try
		{
			addStudent = studentService.addStudent(student);

		}
		catch(Exception ex) 
		{
			throw new CustomException(ex.getMessage(),"Add student operation failed");
		}
		return new ResponseEntity <Student> (addStudent,HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/deleteStudent/{student_id}", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteStudent(@PathVariable("student_id") Integer student_id){
		try 
		{
			studentService.deleteStudent(student_id);

		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage(),"delete studentDetail failed");
		}
		return new ResponseEntity<Void>(HttpStatus.OK); 
	}
	@RequestMapping(value = "/getStudentDetailByName/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<Student>> getStudentDetailByName(@PathVariable("name") String name){
		List<Student> student = null;
		try 
		{
			student = studentService.getStudentDetailsByName(name);
			if(student == null) 
			{
				throw new CustomException("No Data found for this name");
			}

		}
		catch(Exception ex)
		{
			throw new CustomException(ex.getMessage(),"get studentDetails failed");
		}
		return new ResponseEntity<List<Student>>(student, HttpStatus.OK); 
	}
}

