package com.example.studentMicroservice.service;

import java.util.List;

import com.example.studentMicroservice.entity.Student;

public interface StudentService {
public List<Student> getAllStudents();
public Student getStudentDetails(Integer student_id);
public Student addStudent(Student student);
public void deleteStudent(Integer student_id);
public List<Student> getStudentDetailsByName(String name);
}
