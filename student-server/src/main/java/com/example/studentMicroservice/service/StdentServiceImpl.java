package com.example.studentMicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.example.studentMicroservice.entity.Student;
import com.example.studentMicroservice.exception.CustomException;
import com.example.studentMicroservice.repository.StudentRepository;
@Service
public class StdentServiceImpl implements StudentService{
	@Autowired
	private StudentRepository studentRepository;
	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		List<Student> students = null;
		try 
		{
			students=(List<Student>)studentRepository.findAll();
		}
		catch(Exception e) 
		{
			throw e;
		}
		return students;
	}

	@Override
	public Student getStudentDetails(Integer student_id) {
		// TODO Auto-generated method stub
		Student student = null;
		try
		{

				student = studentRepository.findById(student_id).get();

		}
		catch(Exception e)
		{
			throw e;
		}
		return student;
	}

	@Override
	public Student addStudent(Student student) {
		// TODO Auto-generated method stub
		try
		{
			if(student.getStudent_id() != null) 
			{
				Student  oldStudent=studentRepository.findById(student.getStudent_id()).get();
				studentRepository.save(student);
			}
			else 
			{
				studentRepository.save(student);
			}
		}
		catch(DataIntegrityViolationException e) {
			throw new CustomException("Student Already Exist!!!");
		}
		catch(Exception e)
		{
			throw new CustomException("Please try again!!!");
		}

		return student;
	}

	@Override
	public void deleteStudent(Integer student_id) {
		// TODO Auto-generated method stub
		try
		{

				studentRepository.deleteById(student_id);

		}
		catch(Exception e)
		{
			throw e;
		}
	}

	@Override
	public List<Student> getStudentDetailsByName(String name) {
		// TODO Auto-generated method stub
		List<Student> students = null;
		try 
		{
			students=(List<Student>)studentRepository.findStudentsByName(name);
		}
		catch(Exception e) 
		{
			throw e;
		}
		return students;
	}

}
