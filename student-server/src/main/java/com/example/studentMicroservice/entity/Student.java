package com.example.studentMicroservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



	@Entity
	@Table(name="student")
public class Student {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "student_id")
private Integer student_id;
@Column(name="name")
private String name;
@Column(name="age")
private Integer age;
@Column(name="contact")
private String contact;

public Student(Integer student_id, String name, Integer age, String contact) {
	super();
	this.student_id = student_id;
	this.name = name;
	this.age = age;
	this.contact = contact;
}
public Integer getStudent_id() {
	return student_id;
}
public void setStudent_id(Integer student_id) {
	this.student_id = student_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Integer getAge() {
	return age;
}
public void setAge(Integer age) {
	this.age = age;
}
public String getContact() {
	return contact;
}
public void setContact(String contact) {
	this.contact = contact;
}

public Student() {
	super();
	// TODO Auto-generated constructor stub
}

}
