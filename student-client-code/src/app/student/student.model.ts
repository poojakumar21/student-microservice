export class Student {
  public student_id:number;
	public name: string;
	public age: number;
	public contact: string;
  constructor(student_id:number,name:string,age:number,contact:string){
    this.student_id = student_id;
    this.name = name;
    this.age = age;
    this.contact = contact;
  }
}
