import { Component, OnInit } from '@angular/core';
import { Student } from './student.model';
import { StudentService } from './student.service';
import { FormBuilder } from '@angular/forms';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private studentService: StudentService, private router: Router) {
  }
  students: Student[];
  student: Student;
  display: boolean = false;
  index: number;
  displayError: boolean = false;
  message: String;
  sortAge: boolean = false;
  sortName: boolean = false;

  ngOnInit() {
    this.getStudents();
  }
  //to fetch all students data
  getStudents() {
    this.studentService.getAllStudents().subscribe((response) => {
      this.students = response;
    },
      (error) => {
        this.displayError = true;
        this.message = error.error.debugMessage;
      }
    )
  }

//for submitting form data
  onSubmit(studentForm) {
    if (this.display == false) {
      this.student = studentForm.value;
      this.studentService.addStudent(this.student).subscribe((response) => {
        this.student = response;
        studentForm.reset();
        this.getStudents();
      },
        (error) => {
          this.displayError = true;
          this.message = error.error.debugMessage;
        })
    }
    else {

      this.student.name = studentForm.value.name;
      this.student.age = studentForm.value.age;
      this.student.contact = studentForm.value.contact;
      console.log(this.student);

      this.studentService.addStudent(this.student).subscribe((response) => {
        this.student = response;
        studentForm.reset();
        this.getStudents();
        this.display = false;
      },
        (error) => {
          this.displayError = true;
          this.message = error.error.debugMessage;
        })
      console.log(this.student);
    }
    console.log(studentForm.value);
  }

  //for deleting student detail
  delete(id: number) {
    this.index = id;
    console.log(this.index);
    this.studentService.deleteStudentDetail(this.index).subscribe(() => this.getStudents());

  }

  //to enable update button and fetch corresponding data
  edit(id: number) {
    this.display = true;
    this.index = id;
    this.studentService.getStudentDetail(this.index).subscribe((response: Student) => {
      this.student = response;
    },
      (error) => {
        this.displayError = true;
        this.message = error.error.debugMessage;
      })
  }

  //sort list of students by name
  sortByName() {
    this.sortName = !this.sortName;
    if (this.sortName == true) {
      this.students.sort((a, b) => a.name.localeCompare(b.name));
    }
    else {
      this.students.sort((a, b) => b.name.localeCompare(a.name));

    }
  }

  //sort list of students by age
  sortByAge() {
    this.sortAge = !this.sortAge;
    if (this.sortAge == true) {
      this.students.sort((a, b) => a.age-b.age);
    }
    else {
      this.students.sort((a, b) => b.age-a.age);

    }
  }

  //redirect to login page
  logout() {
    console.log("logout");
    this.router.navigate(['/login']);

  }

  //search students list by studentName
  searchByName(searchByStudentName) {
    console.log(searchByStudentName);
    this.studentService.getAllStudentsByName(searchByStudentName).subscribe((response) => {
      if (response.length == 0) {
        this.message = "No Records found";
      }
      this.students = response;
    })
  }

}














