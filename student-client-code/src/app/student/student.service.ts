import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from 'rxjs';
import { Student } from './student.model';

@Injectable({
  providedIn: 'root'
})

export class StudentService {
  constructor(private http: HttpClient) { }

  getAllStudents(): Observable<Student[]> {
    return this.http.get<Student[]>("http://localhost:8080/api/students/getAllStudents");
  }

  addStudent(addStudent: Student): Observable<Student> {
    return this.http.post<Student>("http://localhost:8080/api/students/addStudent", addStudent);
  }

  getStudentDetail(student_id) {
    return this.http.get(`http://localhost:8080/api/students/getStudentDetail/${student_id}`);
  }
  deleteStudentDetail(student_id) {
    return this.http.get(`http://localhost:8080/api/students/deleteStudent/${student_id}`);
  }
  getAllStudentsByName(name): Observable<Student[]> {
    return this.http.get<Student[]>(`http://localhost:8080/api/students/getStudentDetailByName/${name}`);
  }
  // private subject = new Subject<Student[]>();
  // students = [
  //   new Student('a',"12","876767"),
  //   new Student('b',"11","876767"),
  //   new Student('c',"16","876767"),
  //   new Student('d',"18","876767"),

  // ];
  // getAllStudents(){

  //    this.subject.next(this.students);
  //    return this.students;
  //   }


}
